# Gsos Maven Parent

GeoSales - Open Source Maven parent. O parent global a ser herdado pelos projetos open source da GS, e por conveniência de quem mais precisar.

## Como usar

O ciclo de vida dessa dependência e o futuro das demais do GeoSales - Open Source será feito através do GitLab, os artefatos sendo levantados no grupo GeoSales - Open Source.

Adicione o seguinte `repository` no seu POM para poder importar este daqui:

```xml
		<repository>
			<id>gsos-gitlab-maven</id>
			<url>https://gitlab.com/api/v4/groups/2713499/packages/maven</url>
		</repository>
```

As primeiras distribuições dos projetos no GeoSales - Open Source eram feitas via [JitPack](https://jitpack.io). Para habilitar a opção de fazer o download dessas versões anteriores dessas dependências, adicione o seguinte `repository`:

```xml
		<repository>
			<id>jitpack</id>
			<url>https://jitpack.io</url>
		</repository>
```

> Em ambos os casos, o `id` dos `repository` são puramente arbitrários, use o valor que lhe convir.

Após configurar corretamente o `repository`, adicione a menção ao `parent`:

```xml
	<parent>
		<groupId>br.com.softsite</groupId>
		<artifactId>gsos-maven-parent</artifactId>
		<version>1</version>
	</parent>
```
